# Test case description

This test case consists of a 3D box grid, used to simulated a forest 
edge, based on a measurement campaign carried out in Falster. The 
forest is modeled as local source terms in the momentum and turbulence 
equations. The location and magnitude of the sources terms is defined 
by a 3D grid of the plant area density, as specified in the canopy 
file: grid.CAN. For more information, see:
http://dx.doi.org/10.1016/j.agrformet.2014.10.014
and
DOI 10.1007/s10546-006-9073-5

## Grid

The grid consists of 128 x 128 x 64 cells in stream-wise (x), lateral (y) 
and normal (z) directions, respectively. The domain is 5 km long and wide,
and 500 m tall. A neutral profile is set at the inlet (x=0) and at the top
(z=Lz). The side boundaries are periodic boundaries, at x=Lx an outlet is 
defined, and at z=z0, a rough wall condition is specified.

## Case 1: wd285

A wind direction of 285 is specified.


